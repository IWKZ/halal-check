# IWKZ - Hallal Check
check product berdasarkan barcode untuk menentukan apakah product mengandung unsur haram atau tidak.

### Development

To start a development on default port 3000:
* ```$ yarn```
* ```$ yarn start```
* ```$ ngrok http 3000 -host-header="localhost:3000"```

Build:
* ```$ yarn run build```