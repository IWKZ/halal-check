import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Quagga from 'quagga';

import IconButton from '@material-ui/core/IconButton';
import CropFreeIcon from '@material-ui/icons/CropFree';
import { ScannerSection, StartScanner, Content } from './styled.components';

import { SCANNER_CODE_READER } from '../config';

const Scanner = ({
  hideScanner,

  onDetected,
  onError,
  onHideScannerDone,
}) => {
  const [init, onInit] = useState(false);
  const [startScanner, onStartScanner] = useState(false);

  const onClickStartScanner = () => onStartScanner(true);

  useEffect(() => {
    if (!init && startScanner) {
      Quagga.init({
        inputStream: {
          name: 'Live',
          type: 'LiveStream',
          target: document.querySelector('#scanner'),
          constraints: {
            width: window.innerWidth,
            height: window.innerHeight,
          },
        },
        decoder: {
          readers: SCANNER_CODE_READER,
        },
      }, (err) => {
        if (err) {
          onError();
          return;
        }
        onInit(true);
        Quagga.start();
      });
      Quagga.onDetected(onDetected);
    }

    if (hideScanner) {
      Quagga.stop();
      onStartScanner(false);
      onInit(false);
    }

    onHideScannerDone();
  }, [startScanner, onDetected, onError, hideScanner, init, onHideScannerDone]);

  return (
    <ScannerSection>
      {startScanner
        ? (
          <Content>
            <div>
              <div id="scanner" />
            </div>
            <h3>silakan scan barcodenya.</h3>
          </Content>
        ) : (
          <StartScanner>
            <IconButton onClick={onClickStartScanner}>
              <CropFreeIcon fontSize="large" />
            </IconButton>
            <h3>Start Scanner</h3>
          </StartScanner>
        )}
    </ScannerSection>
  );
};

Scanner.propTypes = {
  hideScanner: PropTypes.bool,

  onDetected: PropTypes.func,
  onError: PropTypes.func,
  onHideScannerDone: PropTypes.func,
};

Scanner.defaultProps = {
  hideScanner: false,

  onDetected: () => {},
  onError: () => {},
  onHideScannerDone: () => {},
};

export default Scanner;
