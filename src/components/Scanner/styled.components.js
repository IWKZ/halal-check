import styled from 'styled-components';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

export const ScannerSection = styled(Container).attrs(() => ({
  fixed: true,
}))`
  height: 100%;

  #scanner video, canvas {
    width: 100%;
    height: auto;
  }
`;

export const Content = styled(Grid).attrs(() => ({
  container: true,
  direction: 'column',
  justify: 'center',
  alignItems: 'center',
}))``;

export const StartScanner = styled(Content)`
  height: 100%;

  .MuiSvgIcon-fontSizeLarge {
    font-size: 10em;
  }
`;
