import React, { useState } from 'react';
import Scanner from './Scanner';
import ResultModal from './ResultModal';

const App = () => {
  const [eanCode, setEanCode] = useState(null);
  const [hideScanner, setHideScanner] = useState(false);
  const [showResultModal, setResultModal] = useState(false);

  const onDetected = ({ codeResult }) => {
    const { code } = codeResult;

    if (code) {
      setEanCode(code);
      setHideScanner(true);
      setResultModal(true);
    }
  };

  const onHideScannerDone = () => setHideScanner(false);

  const onCloseResultModal = () => setResultModal(false);

  return (
    <>
      <Scanner
        hideScanner={hideScanner}
        onDetected={onDetected}
        onHideScannerDone={onHideScannerDone} />

      <ResultModal
        showResult={showResultModal}
        eanCode={eanCode}
        onClose={onCloseResultModal} />
    </>
  );
};

export default App;
