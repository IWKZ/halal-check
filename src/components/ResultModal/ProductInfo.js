import React from 'react';
import PropTypes from 'prop-types';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Cancel';
import { StyledContainer, HeaderAction, Content } from './styled.components';

const PRODUCT_STATUS = {
  ALLOWED: 'hallal',
  NOT_ALLOWED: 'haram',
  UNDEFINED: '-',
};

const ProductInfo = ({ productInfo, isAllowedProduct, onClose }) => {
  const {
    imageUrl,
    title,
    status,
    ingredient,
  } = productInfo;

  const getImageUrl = () => {
    if (!imageUrl) return '';

    const splitImageUrl = imageUrl.split('id=');
    const imageId = splitImageUrl[1];

    return `https://drive.google.com/uc?export=view&id=${imageId}`;
  };

  const getStatus = () => {
    let tmpStatus = PRODUCT_STATUS.UNDEFINED;

    if (status === PRODUCT_STATUS.ALLOWED && ingredient) tmpStatus = PRODUCT_STATUS.ALLOWED;
    if (status === PRODUCT_STATUS.NOT_ALLOWED) tmpStatus = PRODUCT_STATUS.NOT_ALLOWED;

    return tmpStatus;
  };

  return (
    <StyledContainer>
      <HeaderAction>
        <IconButton color="primary" onClick={onClose}>
          <CloseIcon />
        </IconButton>
      </HeaderAction>
      <Content className={getStatus()}>
        <div>
          <h2>{title}</h2>
          {
            isAllowedProduct && (
              <h1 className="productStatus">
                { `status: ${getStatus()}` }
              </h1>
            )
          }
        </div>
        {
          !!(getImageUrl()) && (
            <div className="productImage">
              <img src={getImageUrl()} />
            </div>
          )
        }
        <div className="productDescription">
          {
            isAllowedProduct ? (
              <>
                <h2>Komposisi:</h2>
                <p>{ingredient}</p>
              </>
            ) : (<h3>Hati-hati sepertinya product ini tidak bisa dikonsumsi</h3>)
          }
        </div>
      </Content>
    </StyledContainer>
  );
};

ProductInfo.propTypes = {
  productInfo: PropTypes.shape({
    title: PropTypes.string,
    imageUrl: PropTypes.string,
    status: PropTypes.string,
    ingredient: PropTypes.string,
  }),
  isAllowedProduct: PropTypes.bool,

  onClose: PropTypes.func,
};

ProductInfo.defaultProps = {
  productInfo: {
    title: null,
    imageUrl: null,
    status: null,
    ingredient: null,
  },
  isAllowedProduct: true,

  onClose: () => {},
};

export default ProductInfo;
