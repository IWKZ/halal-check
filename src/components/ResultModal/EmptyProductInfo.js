import React from 'react';
import PropTypes from 'prop-types';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Cancel';
import { StyledContainer, HeaderAction, Content } from './styled.components';

const EmptyProductInfo = ({ onClose }) => (
  <StyledContainer>
    <HeaderAction>
      <IconButton color="primary" onClick={onClose}>
        <CloseIcon />
      </IconButton>
    </HeaderAction>
    <Content>
      <div className="productDescription">
        <h3>Maaf kami belum menemukan product yang kamu cari.</h3>
      </div>
    </Content>
  </StyledContainer>
);

EmptyProductInfo.propTypes = {
  onClose: PropTypes.func,
};

EmptyProductInfo.defaultProps = {
  onClose: () => {},
};

export default EmptyProductInfo;
