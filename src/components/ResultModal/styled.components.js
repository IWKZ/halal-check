import styled from 'styled-components';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

export const StyledContainer = styled(Container).attrs(() => ({
  fixed: true,
}))`
  background-color: #90caf9;
  height: 100%;
`;

export const HeaderAction = styled(Grid).attrs(() => ({
  container: true,
  direction: 'row',
  justify: 'flex-end',
  alignItems: 'flex-start',
}))``;


export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify: center;
  align-items: center; 
  height: 100%;
  background-color: #fff;
  border-radius: 10px;
  overflow: scroll;

  &.hallal {
    color: white;
    background-color: #C0D890 !important;
  }

  &.haram {
    color: white;
    background-color: #F1396D;
  }

  .productDescription {
    padding: 20px;
    margin-top: 20px;
    margin-bottom: 20px;
  }

  .productStatus {
    text-align: center;
    text-transform: uppercase;
    font-weight: bolder;
  }

  .productImage {
    height: 30%;
    margin-bottom: 20px;

    &img {
      width: 40%;
    }
  }
`;
