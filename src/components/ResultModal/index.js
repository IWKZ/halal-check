import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import Modal from '@material-ui/core/Modal';
import LoadingOverlay from '../LoadingOverlay';
import EmptyProductInfo from './EmptyProductInfo';
import ProductInfo from './ProductInfo';

import { getBarcodeData } from './ApiService';

class ResultModal extends Component {
  constructor() {
    super();

    this.state = {
      loading: false,
      currentEanCode: null,
      productInfo: {},
      isAllowedProduct: true,
    };
  }

  componentDidUpdate() {
    this.reactOnNewEanCode();
  }

  get isProductInfoEmpty() {
    const { productInfo } = this.state;

    return isEmpty(productInfo) || productInfo.notFound;
  }

  reactOnNewEanCode = () => {
    const { currentEanCode } = this.state;
    const { eanCode } = this.props;

    if (eanCode !== currentEanCode) {
      this.setState({
        loading: true,
        productInfo: {},
        currentEanCode: eanCode,
        isAllowedProduct: true,
      });

      getBarcodeData(eanCode, ({ success, data }) => {
        const newState = { loading: false };

        if (success) {
          newState.productInfo = data;
          newState.isAllowedProduct = !data.notAllowed;
        }

        this.setState(newState);
      });
    }
  }

  render() {
    const { showResult, onClose } = this.props;
    const { loading, productInfo, isAllowedProduct } = this.state;

    return (
      <Modal
        open={showResult}
        onClose={onClose}>
        <LoadingOverlay loading={loading}>
          {
            this.isProductInfoEmpty
              ? (
                <EmptyProductInfo
                  onClose={onClose} />
              )
              : (
                <ProductInfo
                  isAllowedProduct={isAllowedProduct}
                  productInfo={productInfo}
                  onClose={onClose} />
              )
          }
        </LoadingOverlay>
      </Modal>
    );
  }
}

ResultModal.propTypes = {
  showResult: PropTypes.bool,
  eanCode: PropTypes.string,

  onClose: PropTypes.func,
};

ResultModal.defaultProps = {
  showResult: false,
  eanCode: null,

  onClose: () => {},
};

export default ResultModal;
