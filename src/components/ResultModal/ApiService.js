import axios from 'axios';
import { API_URL, DEFAULT_SERVER_TIMOUT } from '../config';

const responseTemplate = (data) => ({
  success: !!(Object.keys(data).length),
  data,
});

export const getBarcodeData = (eanCode, cb) => {
  if (!eanCode) cb(responseTemplate({}));

  const url = `${API_URL}/product/${eanCode}`;

  axios({
    method: 'GET',
    url,
    timeout: DEFAULT_SERVER_TIMOUT,
  }).then(({ data }) => cb(responseTemplate(data)))
    .catch(() => cb(responseTemplate({})));
};
